package cn.yawei;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class MyFormTest {
	@RequestMapping("/test")
	@Token(remove = true)
	public ModelAndView formTest(String username, String password) {
		System.out.println(username + "===" + password + "===" + new Date());
		ModelAndView modelAndView = new ModelAndView("data");
		modelAndView.addObject("data", username + "===" + password + "===" + new Date());
		return modelAndView;
	}
	
	@RequestMapping("/login")
	@Token(save = true)
	public String formLogin(HttpServletRequest request, HttpServletResponse response) {
		return "login";
	}
}
